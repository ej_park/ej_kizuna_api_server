package tokyo.ejservice.kizuna.service.contents;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import tokyo.ejservice.kizuna.core.AmazonS3Helper;
import tokyo.ejservice.kizuna.core.ApiRequestTemplate;
import tokyo.ejservice.kizuna.service.RequestParamException;
import tokyo.ejservice.kizuna.service.ServiceException;
import tokyo.ejservice.kizuna.service.dao.ContentsDAO;

@Service("contentsAdd")
@Scope("prototype")
public class ContentsAdd extends ApiRequestTemplate {
	@Resource(name = "contentsDAO")
	private ContentsDAO contentsDAO;

	public ContentsAdd(Map<String, String> reqData) {
		super(reqData);
	}

	@Override
	public void requestParamValidation() throws RequestParamException {
		if (this.reqData.get("userNo") == null) {
			throw new RequestParamException("The parameter(userNo) is not exist.");
		}

		/*if (this.reqData.get("comment") == null && this.reqData.get("pic") == null) {
			throw new RequestParamException("The parameter(comment|pic) is not exist.");
		}*/

	}

	@Override
	public void service() throws ServiceException {
		// FIXME contentsNoでpicUrl Key作成
		String pic = this.reqData.get("pic");
		if (pic != null) {
			long nowDate = System.currentTimeMillis(); // unixtime
			String key = "contents/" + this.reqData.get("userNo") + "/pic_" + nowDate;
			AmazonS3Helper.getInstance().upload(key, pic);
			this.reqData.put("picUrl", key);
		}
		int count = contentsDAO.addContents(this.reqData);
		if (count > 0) {
			// commentテーブルとの連動のため、myBatisのkeyProperty使用
			// @keyword mybatis mysql insert selectkey
			String comment = this.reqData.get("comment");
			// TODO contentsとcommentテーブルのtransaction処理
			if (comment != null) {
				count = contentsDAO.addComment(this.reqData);
			}

			if (count > 0) {
				this.apiResult.addProperty("resultCode", 200);
				this.apiResult.addProperty("message", "Success");
				return;
			}
		}

		// 追加失敗
		this.apiResult.addProperty("resultCode", 405);
		this.apiResult.addProperty("message", "Failed to add contents.");
	}

}
