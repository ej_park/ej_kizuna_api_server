package tokyo.ejservice.kizuna.service.contents;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import tokyo.ejservice.kizuna.core.ApiRequestTemplate;
import tokyo.ejservice.kizuna.service.RequestParamException;
import tokyo.ejservice.kizuna.service.ServiceException;
import tokyo.ejservice.kizuna.service.dao.ContentsDAO;

@Service("commentAdd")
@Scope("prototype")
public class CommentAdd extends ApiRequestTemplate {
	@Resource(name = "contentsDAO")
	private ContentsDAO contentsDAO;

	public CommentAdd(Map<String, String> reqData) {
		super(reqData);
	}

	@Override
	public void requestParamValidation() throws RequestParamException {
		if (this.reqData.get("userNo") == null) {
			throw new RequestParamException("The parameter(userNo) is not exist.");
		}

		if (this.reqData.get("comment") == null && this.reqData.get("readIcon") == null) {
			throw new RequestParamException("The parameter(comment|readIcon) is not exist.");
		}

	}

	@Override
	public void service() throws ServiceException {
		int count = 0;
		// 購読アイコン追加
		if (this.reqData.get("readIcon") != null) {
			count = contentsDAO.addRead(this.reqData);
		}
		// コメント追加
		else if (this.reqData.get("comment") != null) {
			count = contentsDAO.addComment(this.reqData);
		}

		if (count > 0) {
			this.apiResult.addProperty("resultCode", 200);
			this.apiResult.addProperty("message", "Success");
		} else {
			// 追加失敗
			this.apiResult.addProperty("resultCode", 405);
			this.apiResult.addProperty("message", "Failed to add contents.");
		}
	}

}
