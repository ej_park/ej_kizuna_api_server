package tokyo.ejservice.kizuna.service.contents;

import java.math.BigDecimal;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import tokyo.ejservice.kizuna.core.AmazonS3Helper;
import tokyo.ejservice.kizuna.core.ApiRequestTemplate;
import tokyo.ejservice.kizuna.core.TokenHelper;
import tokyo.ejservice.kizuna.service.RequestParamException;
import tokyo.ejservice.kizuna.service.ServiceException;
import tokyo.ejservice.kizuna.service.dao.ContentsDAO;

@Service("contentsGet")
@Scope("prototype")
public class ContentsGet extends ApiRequestTemplate  {
	@Resource(name = "contentsDAO")
	private ContentsDAO contentsDAO;

	private Map<String, String> urlMap = null;

	public ContentsGet(Map<String, String> reqData) {
		super(reqData);
	}

	@Override
	public void requestParamValidation() throws RequestParamException {
		if (this.reqData.get("offset") == null) {
			throw new RequestParamException("The parameter(offset) is not exist.");
		}

		if (this.reqData.get("limit") == null) {
			throw new RequestParamException("The parameter(limit) is not exist.");
		}
	}

	@Override
	public void service() throws ServiceException {
		List<Map<String, Object>> result = null;
		String userNo = this.reqData.get("userNo");
		if (userNo != null) {
			result = contentsDAO.getContentsByUserNo(this.reqData);
		} else {
			result = contentsDAO.getContents(this.reqData);
		}
		if (result != null && result.size() > 0) {
			// make presigned url
			String tokenKey = this.reqData.get("token");
			long expireMillisec = TokenHelper.getInstance().getExpireMillisec(tokenKey);
			urlMap = new HashMap<String, String>();

			JsonArray contentsList = new JsonArray();
			for(Map<String, Object> item : result) {
				String contentsNo = String.valueOf(item.get("contents_no"));
				this.reqData.put("contentsNo", contentsNo);

				String userPicUrl = getPresignedUrl(String.class.cast(item.get("user_pic_url")), expireMillisec);
				String picUrl = getPresignedUrl(String.class.cast(item.get("pic_url")), expireMillisec);

				JsonObject contents = new JsonObject();
				contents.addProperty("contentsNo", Long.class.cast(item.get("user_no")));
				contents.addProperty("userNo", Long.class.cast(item.get("user_no")));
				contents.addProperty("userPicUrl", userPicUrl);
				contents.addProperty("familyName", String.class.cast(item.get("family_name")));
				contents.addProperty("firstName", String.class.cast(item.get("first_name")));
				contents.addProperty("picUrl", picUrl);
				contents.addProperty("createDate", String.class.cast(DATE_FORMAT.format(item.get("create_date"))));

				//readList
				JsonArray readList = new JsonArray();
				List<Map<String, Object>> readResult = contentsDAO.getRead(this.reqData);
				for(Map<String, Object> readItem : readResult) {
					userPicUrl = getPresignedUrl(String.class.cast(readItem.get("user_pic_url")), expireMillisec);

					JsonObject read = new JsonObject();
					read.addProperty("userNo", Long.class.cast(readItem.get("user_no")));
					read.addProperty("userPicUrl", userPicUrl);
					read.addProperty("readIcon", Integer.class.cast(readItem.get("read_icon")));
					read.addProperty("createDate", String.class.cast(DATE_FORMAT.format(item.get("create_date"))));
					readList.add(read);
				}
				contents.add("readList", readList);

				//commentList
				JsonArray commentList = new JsonArray();
				List<Map<String, Object>> commentResult = contentsDAO.getConmment(this.reqData);
				for(Map<String, Object> commentItem : commentResult) {
					userPicUrl = getPresignedUrl(String.class.cast(commentItem.get("user_pic_url")), expireMillisec);

					JsonObject comment = new JsonObject();
					comment.addProperty("userNo", Long.class.cast(commentItem.get("user_no")));
					comment.addProperty("userPicUrl", userPicUrl);
					comment.addProperty("familyName", String.class.cast(commentItem.get("family_name")));
					comment.addProperty("firstName", String.class.cast(commentItem.get("first_name")));
					comment.addProperty("comment", String.class.cast(commentItem.get("comment")));
					comment.addProperty("lat", BigDecimal.class.cast(commentItem.get("lat")));
					comment.addProperty("lon", BigDecimal.class.cast(commentItem.get("lon")));
					comment.addProperty("createDate", String.class.cast(DATE_FORMAT.format(item.get("create_date"))));
					commentList.add(comment);
				}
				contents.add("commentList", commentList);
				contentsList.add(contents);
			}

			// helper.
			this.apiResult.addProperty("resultCode", 200);
			this.apiResult.addProperty("message", "Success");
			this.apiResult.add("contentsList", contentsList);

		} else {
			// 存在しない
			this.apiResult.addProperty("resultCode", "404");
			this.apiResult.addProperty("message", "The user is not exist.");
		}

	}

	private String getPresignedUrl(String url, long expireMillisec) {
		String presignedUrl = null;
		if (url == null) return null;
		//　重複URLは取得しない
		if (!this.urlMap.containsKey(url)) {
			URL presignedUrlObj = AmazonS3Helper.getInstance().getPresignedUrl(url, expireMillisec);
			if (presignedUrlObj != null) presignedUrl = presignedUrlObj.toExternalForm();
			this.urlMap.put(url, presignedUrl);
		} else {
			presignedUrl = this.urlMap.get(url);
		}
		return presignedUrl;
	}

}
