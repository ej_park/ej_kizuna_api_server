package tokyo.ejservice.kizuna.service.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository("userDAO")
public class UserDAO extends AbstractDAO {

	@SuppressWarnings("unchecked")
	public Map<String, Object> getUserByUserNo(Map<String, String> reqData) {
		return (Map<String, Object>) selectOne("user.userInfoByUserNo", reqData);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getUserByEmail(Map<String, String> reqData) {
		return (Map<String, Object>) selectOne("user.userInfoByEmail", reqData);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> loginUser(Map<String, String> reqData) {
		return (Map<String, Object>) selectOne("user.login", reqData);
	}

	public int addUser(Map<String, String> reqData) {
		return (int) insert("user.addUser", reqData);
	}

	public int updateUser(Map<String, String> reqData) {
		return (int) update("user.updateUser", reqData);
	}

	// logical delete
	public int deleteUser(Map<String, String> reqData) {
		return (int) update("user.delUser", reqData);
	}
}
