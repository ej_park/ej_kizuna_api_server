package tokyo.ejservice.kizuna.service.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

@Repository("contentsDAO")
public class ContentsDAO extends AbstractDAO {

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getContents(Map<String, String> reqData) {
		int offset = Integer.parseInt(reqData.get("offset"));
		int limit = Integer.parseInt(reqData.get("limit"));
		RowBounds rowBounds = new RowBounds(offset, limit);
		return (List<Map<String, Object>>) selectList("contents.getContents", reqData, rowBounds);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getContentsByUserNo(Map<String, String> reqData) {
		int offset = Integer.parseInt(reqData.get("offset"));
		int limit = Integer.parseInt(reqData.get("limit"));
		RowBounds rowBounds = new RowBounds(offset, limit);
		return (List<Map<String, Object>>) selectList("contents.getContentsByUserNo", reqData, rowBounds);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getConmment(Map<String, String> reqData) {
		return (List<Map<String, Object>>) selectList("contents.getComment", reqData);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getRead(Map<String, String> reqData) {
		return (List<Map<String, Object>>) selectList("contents.getRead", reqData);
	}

	public int addContents(Map<String, String> reqData) {
		return (int) insert("contents.addContents", reqData);
	}

	public int addComment(Map<String, String> reqData) {
		return (int) insert("contents.addComment", reqData);
	}

	public int addRead(Map<String, String> reqData) {
		return (int) insert("contents.addRead", reqData);
	}

	public int updateRead(Map<String, String> reqData) {
		return (int) update("contents.updateRead", reqData);
	}
}
