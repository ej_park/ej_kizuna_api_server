package tokyo.ejservice.kizuna.service.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository("friendDAO")
public class FriendDAO extends AbstractDAO {

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getFriends(Map<String, String> reqData) {
		return (List<Map<String, Object>>) selectList("friend.getFriends", reqData);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getFriend(Map<String, String> reqData) {
		return (Map<String, Object>) selectOne("friend.getFriend", reqData);
	}

	public int addFreind(Map<String, String> reqData) {
		return (int) insert("friend.addFriend", reqData);
	}

	public int updateFreind(Map<String, String> reqData) {
		return (int) update("friend.updateFriend", reqData);
	}
}
