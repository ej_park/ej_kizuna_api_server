package tokyo.ejservice.kizuna.service.user;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import tokyo.ejservice.kizuna.core.AmazonS3Helper;
import tokyo.ejservice.kizuna.core.ApiRequestTemplate;
import tokyo.ejservice.kizuna.service.RequestParamException;
import tokyo.ejservice.kizuna.service.ServiceException;
import tokyo.ejservice.kizuna.service.dao.UserDAO;

@Service("userUpdate")
@Scope("prototype")
public class UserUpdate extends ApiRequestTemplate {
	@Resource(name = "userDAO")
	private UserDAO userDAO;

	public UserUpdate(Map<String, String> reqData) {
		super(reqData);
	}

	@Override
	public void requestParamValidation() throws RequestParamException {
		if (StringUtils.isEmpty(this.reqData.get("userNo"))) {
			throw new RequestParamException("The parameter(userNo) is not exist.");
		}
	}

	@Override
	public void service() throws ServiceException {
		// check the user is exist.
		Map<String, Object> result = userDAO.getUserByUserNo(this.reqData);
		if (result != null) {
			// image upload to AmazonS3
			String pic = this.reqData.get("pic");
			String bgPic = this.reqData.get("bgPic");
			if (pic != null) {
				String key = "user/" + this.reqData.get("userNo") + "/pic";
				AmazonS3Helper.getInstance().upload(key, pic);
				this.reqData.put("picUrl", key);
			}
			if (bgPic != null) {
				String key = "user/" + this.reqData.get("userNo") + "/bgPic";
				AmazonS3Helper.getInstance().upload(key, bgPic);
				this.reqData.put("bgPicUrl", key);
			}
			int count = userDAO.updateUser(this.reqData);
			if (count > 0) {
				// helper.
				this.apiResult.addProperty("resultCode", "200");
				this.apiResult.addProperty("message", "Success");

				// TODO add token
				// String token = getToken();
				// this.apiResult.addProperty("token", token);
			} else {
				// 更新失敗
				this.apiResult.addProperty("resultCode", "405");
				this.apiResult.addProperty("message", "Failed to update user.");
			}
		} else {
			// 存在しない
			this.apiResult.addProperty("resultCode", "404");
			this.apiResult.addProperty("message", "The user is not exist.");
		}
	}

}
