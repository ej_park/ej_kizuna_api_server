package tokyo.ejservice.kizuna.service.user;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import tokyo.ejservice.kizuna.core.ApiRequestTemplate;
import tokyo.ejservice.kizuna.core.TokenHelper;
import tokyo.ejservice.kizuna.service.RequestParamException;
import tokyo.ejservice.kizuna.service.ServiceException;
import tokyo.ejservice.kizuna.service.dao.UserDAO;

@Service("userLogin")
@Scope("prototype")
public class UserLogin extends ApiRequestTemplate {
	@Resource(name = "userDAO")
	private UserDAO userDAO;

	public UserLogin(Map<String, String> reqData) {
		super(reqData);
	}

	@Override
	public void requestParamValidation() throws RequestParamException {
		if (StringUtils.isEmpty(this.reqData.get("email"))) {
			throw new RequestParamException("The parameter(email) is not exist.");
		}

		if (StringUtils.isEmpty(this.reqData.get("password"))) {
			throw new RequestParamException("The parameter(password) is not exist.");
		}
	}

	@Override
	public void service() throws ServiceException {
		// check the user is exist.
		Map<String, Object> result = userDAO.loginUser(this.reqData);
		if (result != null) {
			// TODO compare password

			String userNo = String.valueOf(result.get("no"));
			String email = String.valueOf(result.get("email"));
			String token = TokenHelper.getInstance().createToken(userNo, email);
			if (token != null) {
				// helper.
				this.apiResult.addProperty("resultCode", "200");
				this.apiResult.addProperty("message", "Success");
				this.apiResult.addProperty("userNo", userNo);
				this.apiResult.addProperty("token", token);
			} else {
				// helper.
				this.apiResult.addProperty("resultCode", "405");
				this.apiResult.addProperty("message", "The token is not create.");
			}
		} else {
			// 存在しない
			this.apiResult.addProperty("resultCode", "404");
			this.apiResult.addProperty("message", "The user is not exist.");
		}
	}
}
