package tokyo.ejservice.kizuna.service.user;

import java.net.URL;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import tokyo.ejservice.kizuna.core.AmazonS3Helper;
import tokyo.ejservice.kizuna.core.ApiRequestTemplate;
import tokyo.ejservice.kizuna.core.TokenHelper;
import tokyo.ejservice.kizuna.service.RequestParamException;
import tokyo.ejservice.kizuna.service.ServiceException;
import tokyo.ejservice.kizuna.service.dao.UserDAO;

@Service("userGet")
@Scope("prototype")
public class UserGet extends ApiRequestTemplate {
	@Resource(name = "userDAO")
	private UserDAO userDAO;

	public UserGet(Map<String, String> reqData) {
		super(reqData);
	}

	@Override
	public void requestParamValidation() throws RequestParamException {
		if (StringUtils.isEmpty(this.reqData.get("userNo"))) {
			throw new RequestParamException("The parameter(userNo) is not exist.");
		}
	}

	@Override
	public void service() throws ServiceException {
		// check the user is already exist.
		Map<String, Object> result = userDAO.getUserByUserNo(this.reqData);
		if (result != null) {
			String email = String.valueOf(result.get("email"));
			String sexKey =  String.class.cast(result.get("sex"));
			int sex = SEX.containsKey(sexKey) ? SEX.get(sexKey) : 0;
			String birthday = String.class.cast(result.get("birthday"));
			String firstName = String.class.cast(result.get("first_name"));
			String familyName = String.class.cast(result.get("family_name"));

			// make presigned url
			String tokenKey = this.reqData.get("token");
			long expireMillisec = TokenHelper.getInstance().getExpireMillisec(tokenKey);
			URL presignedPicUrl = AmazonS3Helper.getInstance().getPresignedUrl(String.class.cast(result.get("pic_url")), expireMillisec);
			String picUrl = (presignedPicUrl == null) ? null : presignedPicUrl.toExternalForm();
			URL presignedBgPicUrl = AmazonS3Helper.getInstance().getPresignedUrl(String.class.cast(result.get("bg_pic_url")), expireMillisec);
			String bgPicUrl = (presignedBgPicUrl == null) ? null : presignedBgPicUrl.toExternalForm();

			// helper.
			this.apiResult.addProperty("resultCode", 200);
			this.apiResult.addProperty("message", "Success");
			this.apiResult.addProperty("email", email);
			this.apiResult.addProperty("sex", sex);
			this.apiResult.addProperty("birthday", birthday);
			this.apiResult.addProperty("firstName", firstName);
			this.apiResult.addProperty("familyName", familyName);
			this.apiResult.addProperty("picUrl", picUrl);
			this.apiResult.addProperty("bgPicUrl", bgPicUrl);

		} else {
			// 存在しない
			this.apiResult.addProperty("resultCode", "404");
			this.apiResult.addProperty("message", "The user is not exist.");
		}
	}

}
