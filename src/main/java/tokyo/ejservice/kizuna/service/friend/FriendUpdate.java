package tokyo.ejservice.kizuna.service.friend;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import tokyo.ejservice.kizuna.core.ApiRequestTemplate;
import tokyo.ejservice.kizuna.service.RequestParamException;
import tokyo.ejservice.kizuna.service.ServiceException;
import tokyo.ejservice.kizuna.service.dao.FriendDAO;

@Service("friendUpdate")
@Scope("prototype")
public class FriendUpdate extends ApiRequestTemplate {
	@Resource(name = "friendDAO")
	private FriendDAO friendDAO;

	public FriendUpdate(Map<String, String> reqData) {
		super(reqData);
	}

	@Override
	public void requestParamValidation() throws RequestParamException {
		if (StringUtils.isEmpty(this.reqData.get("friendUserNo"))) {
			throw new RequestParamException("The parameter(friendUserNo) is not exist.");
		}

	}

	@Override
	public void service() throws ServiceException {
		Map<String, Object> result = friendDAO.getFriend(this.reqData);
		if(result != null) {
			int count = friendDAO.updateFreind(this.reqData);
			if (count > 0) {
				// helper.
				this.apiResult.addProperty("resultCode", "200");
				this.apiResult.addProperty("message", "Success");
			} else {
				// 更新失敗
				this.apiResult.addProperty("resultCode", "405");
				this.apiResult.addProperty("message", "Failed to update friend user.");
			}
		} else {
			// 存在しないユーザ
			this.apiResult.addProperty("resultCode", "404");
			this.apiResult.addProperty("message", "The friend user is not exist.");
		}
	}

}
