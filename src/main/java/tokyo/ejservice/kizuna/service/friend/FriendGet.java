package tokyo.ejservice.kizuna.service.friend;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import tokyo.ejservice.kizuna.core.ApiRequestTemplate;
import tokyo.ejservice.kizuna.service.RequestParamException;
import tokyo.ejservice.kizuna.service.ServiceException;
import tokyo.ejservice.kizuna.service.dao.FriendDAO;

@Service("friendGet")
@Scope("prototype")
public class FriendGet extends ApiRequestTemplate {
	@Resource(name = "friendDAO")
	private FriendDAO friendDAO;

	public FriendGet(Map<String, String> reqData) {
		super(reqData);
	}

	@Override
	public void requestParamValidation() throws RequestParamException {
		//
	}

	@Override
	public void service() throws ServiceException {
		List<Map<String, Object>> result = friendDAO.getFriends(this.reqData);
		if (result != null && result.size() > 0) {
			JsonArray friendList = new JsonArray();
			for(Map<String, Object> item : result) {
				String favFlgKey = String.class.cast(item.get("fav_flg"));
				boolean isFav = BOOL.containsKey(favFlgKey) ? BOOL.get(favFlgKey) : false;

				JsonObject friend = new JsonObject();
				friend.addProperty("userNo", Long.class.cast(item.get("user_no")));
				friend.addProperty("order", Long.class.cast(item.get("order")));
				friend.addProperty("favFlg", isFav);
				friendList.add(friend);
			}
			// helper.
			this.apiResult.addProperty("resultCode", 200);
			this.apiResult.addProperty("message", "Success");
			this.apiResult.add("friendList", friendList);
		} else {
			// 存在しない
			this.apiResult.addProperty("resultCode", "404");
			this.apiResult.addProperty("message", "The friend user is not exist.");
		}
	}

}
