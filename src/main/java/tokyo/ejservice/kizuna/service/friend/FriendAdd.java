package tokyo.ejservice.kizuna.service.friend;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import tokyo.ejservice.kizuna.core.ApiRequestTemplate;
import tokyo.ejservice.kizuna.service.RequestParamException;
import tokyo.ejservice.kizuna.service.ServiceException;
import tokyo.ejservice.kizuna.service.dao.FriendDAO;

@Service("friendAdd")
@Scope("prototype")
public class FriendAdd extends ApiRequestTemplate {
	@Resource(name = "friendDAO")
	private FriendDAO friendDAO;

	public FriendAdd(Map<String, String> reqData) {
		super(reqData);
	}

	@Override
	public void requestParamValidation() throws RequestParamException {
		if (StringUtils.isEmpty(this.reqData.get("friendUserNo"))) {
			throw new RequestParamException("The parameter(friendUserNo) is not exist.");
		}
	}

	@Override
	public void service() throws ServiceException {
		Map<String, Object> result = friendDAO.getFriend(this.reqData);
		if(result == null) {
			int count = friendDAO.addFreind(this.reqData);
			if (count > 0) {
				// helper.
				this.apiResult.addProperty("resultCode", "200");
				this.apiResult.addProperty("message", "Success");
			} else {
				// データなし
				this.apiResult.addProperty("resultCode", "405");
				this.apiResult.addProperty("message", "Failed to add friend user.");
			}
		} else {
			// 既に存在
			this.apiResult.addProperty("resultCode", "400");
			this.apiResult.addProperty("message", "The friend user is alread exist.");
		}
	}

}
