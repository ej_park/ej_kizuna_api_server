package tokyo.ejservice.kizuna.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AbortMultipartUploadRequest;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.PartETag;
import com.amazonaws.services.s3.model.UploadPartRequest;

@Component
public class AmazonS3Helper {
	@Autowired
	@Qualifier("bucketName")
	private String bucketName;

	@Autowired
	@Qualifier("region")
	private String region;

	@Autowired
	@Qualifier("Credentials")
	private BasicAWSCredentials credentials;

	/**
     * log4j logger
     */
    private static final Logger logger = LogManager.getLogger(AmazonS3Helper.class);

    // for singleton
    private static AmazonS3Helper instance = null;
    private AmazonS3 s3Client;

    //とりあえず
    public static String AWS_DOMAIN = "https://prk.s3-ap-northeast-1.amazonaws.com/";

    /**
	 * For connecting to amazon s3. Singleton
	 */
	private AmazonS3Helper() {
		bucketName = "prk";
		credentials = new BasicAWSCredentials("AKIAIRTOFANM3EMZ3ZRA", "aqUfGjxFJdyshKXRhzevIPz0szHXPNdHXSOX4Tgk");
		s3Client = new AmazonS3Client(credentials);
		Region region = Region.getRegion(Regions.AP_NORTHEAST_1);
		s3Client.setRegion(region);
		logger.debug("bucketName:" + bucketName);

		/*try {

			 *
			 * // System.out.println("Creating bucket " + bucketName + "\n"); //
			 * s3.createBucket(bucketName); /* List the buckets in your account

			System.out.println("Listing buckets");
			for (Bucket bucket : s3.listBuckets()) {
				System.out.println(" - " + bucket.getName());
			}
			System.out.println();

			System.out.println("Uploading a new object to S3 from a file\n");
			File logfile = new File("/home/trieu/data/logs/" + key);
			System.out.println("file size: " + logfile.getTotalSpace());
			s3.putObject(new PutObjectRequest(bucketName, key, logfile));


			 * Download an object - When you download an object, you get all of
			 * the object's metadata and a stream from which to read the
			 * contents. It's important to read the contents of the stream as
			 * quickly as possibly since the data is streamed directly from
			 * Amazon S3 and your network connection will remain open until you
			 * read all the data or close the input stream.
			 *
			 * GetObjectRequest also supports several other options, including
			 * conditional downloading of objects based on modification times,
			 * ETags, and selectively downloading a range of an object.

			System.out.println("Downloading an object");
			S3Object object = s3.getObject(new GetObjectRequest(bucketName, key));
			System.out.println("Content-Type: " + object.getObjectMetadata().getContentType());
			displayTextInputStream(object.getObjectContent());


			 * List objects in your bucket by prefix - There are many options
			 * for listing the objects in your bucket. Keep in mind that buckets
			 * with many objects might truncate their results when listing their
			 * objects, so be sure to check if the returned object listing is
			 * truncated, and use the AmazonS3.listNextBatchOfObjects(...)
			 * operation to retrieve additional results.

			System.out.println("Listing objects");
			ObjectListing objectListing = s3.listObjects(
					new ListObjectsRequest().withBucketName(bucketName).withPrefix("localserver_access.log"));
			for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
				System.out.println(" - " + objectSummary.getKey() + "  " + "(size = " + objectSummary.getSize() + ")");
			}
			System.out.println();


			 * Delete an object - Unless versioning has been turned on for your
			 * bucket, there is no way to undelete an object, so use caution
			 * when deleting objects.

			// System.out.println("Deleting an object\n");
			// s3.deleteObject(bucketName, key);


			 * Delete a bucket - A bucket must be completely empty before it can
			 * be deleted, so remember to delete any objects from your buckets
			 * before you try to delete them.

			// System.out.println("Deleting bucket " + bucketName + "\n");
			// s3.deleteBucket(bucketName);
		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch (Exception e) {

		}*/
	}

	public static AmazonS3Helper getInstance() {
		if(instance == null) instance = new AmazonS3Helper();
		return instance;
	}

	/**
	 * Using the AWS SDK for Java for Multipart Upload
	 * (Low-Level implement)
	 * @see http://docs.aws.amazon.com/AmazonS3/latest/dev/UsingMPDotJavaAPI.html
	 * @param keyName
	 * @param filePath
	 */
	public void upload(String keyName, String filePath) {
		File file = new File(filePath);

		// Create a list of UploadPartResponse objects. You get one of these
		// for each part upload.
		List<PartETag> partETags = new ArrayList<PartETag>();

		// Step 1: Initialize.
		InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(bucketName, keyName);
		InitiateMultipartUploadResult initResponse = s3Client.initiateMultipartUpload(initRequest);

		//File file = new File(filePath);
		long contentLength = file.length();
		long partSize = 5242880; // Set part size to 5 MB.

		try {
			// Step 2: Upload parts.
			long filePosition = 0;
			for (int i = 1; filePosition < contentLength; i++) {
				// Last part can be less than 5 MB. Adjust part size.
				partSize = Math.min(partSize, (contentLength - filePosition));

				// Create request to upload a part.
				UploadPartRequest uploadRequest = new UploadPartRequest().withBucketName(bucketName)
						.withKey(keyName).withUploadId(initResponse.getUploadId()).withPartNumber(i)
						.withFileOffset(filePosition).withFile(file).withPartSize(partSize);

				// Upload part and add response to our list.
				partETags.add(s3Client.uploadPart(uploadRequest).getPartETag());

				filePosition += partSize;
			}

			// Step 3: Complete.
			CompleteMultipartUploadRequest compRequest = new CompleteMultipartUploadRequest(bucketName,
					keyName, initResponse.getUploadId(), partETags);

			s3Client.completeMultipartUpload(compRequest);
		} catch (Exception e) {
			logger.error(e);
			s3Client.abortMultipartUpload(new AbortMultipartUploadRequest(bucketName, keyName, initResponse.getUploadId()));
		}
	}

	public URL getPresignedUrl(String keyName, long expireMillisec) {
		URL url = null;
		try {
			if(keyName == null) {
				return null;
			}
			Date expiration = new Date();
			if (expireMillisec == 0) {
				expireMillisec = expiration.getTime();
				expireMillisec += 1000 * 60 * 60; // 1 hour.
			}
			expiration.setTime(expireMillisec);

			GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, keyName);
			generatePresignedUrlRequest.setMethod(HttpMethod.GET); // Default.
			generatePresignedUrlRequest.setExpiration(expiration);

			url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
		} catch (Exception e) {
			logger.error(e);
		}
		return url;
	}

	/**
	 * Displays the contents of the specified input stream as text.
	 *
	 * @param input
	 *            The input stream to display as text.
	 *
	 * @throws IOException
	 */
	void displayTextInputStream(InputStream input) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		while (true) {
			String line = reader.readLine();
			if (line == null)
				break;

			System.out.println("    " + line);
		}
		System.out.println();
	}

}
