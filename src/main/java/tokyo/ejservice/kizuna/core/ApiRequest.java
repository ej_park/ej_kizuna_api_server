package tokyo.ejservice.kizuna.core;

import com.google.gson.JsonObject;

import tokyo.ejservice.kizuna.service.RequestParamException;
import tokyo.ejservice.kizuna.service.ServiceException;

/**
 * API Service request worker interface
 * 
 * @author park
 */
public interface ApiRequest {
    /**
     * リクエストのパラメータの有効性チェック
     * 
     * @throws RequestParamException
     */
    public void requestParamValidation() throws RequestParamException;

    /**
     * AIPサービス実装
     * 
     * @throws Exception
     */
    public void service() throws ServiceException;

    /**
     * APIサービス実行
     * 
     * @throws Exception
     */
    public void executeService();

    /**
     * APIサービス処理結果返還
     * 
     * @return
     */
    public JsonObject getApiResult();
}

