package tokyo.ejservice.kizuna.core;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import tokyo.ejservice.kizuna.service.RequestParamException;

@Service("notFound")
@Scope("prototype")
public class DefaultApiRequest extends ApiRequestTemplate {

    public DefaultApiRequest(Map<String, String> reqData) {
        super(reqData);
    }

    @Override
    public void requestParamValidation() throws RequestParamException {
    	//do something
    }

    @Override
    public void service() {
        this.apiResult.addProperty("resultCode", 404);
    }
}