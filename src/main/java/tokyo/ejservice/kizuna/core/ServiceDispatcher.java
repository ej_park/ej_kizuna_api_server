package tokyo.ejservice.kizuna.core;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Service class dispatcher by URI
 *
 * @author park
 */
@Component
public class ServiceDispatcher {
	private static ApplicationContext springContext;

	@Autowired
	public void init(ApplicationContext springContext) {
		ServiceDispatcher.springContext = springContext;
	}

	protected Logger logger = LogManager.getLogger(this.getClass());

	public static ApiRequest dispatch(Map<String, String> reqData) {
		ApiRequest service = null;
		try {
			String serviceUri = reqData.get("REQUEST_URI");
			String httpMethod = reqData.get("REQUEST_METHOD");
			String beanName = null;
			String[] uriSegments = null;
			if (serviceUri == null || serviceUri.length() == 0) {
				throw new Exception("serviceUri is null.");
			}

			// uri:/users/login
			uriSegments = serviceUri.split("/");
			if (uriSegments == null || uriSegments.length < 2) {
				throw new Exception("serviceUri is not valid.");
			}

			switch (uriSegments[1]) {

			case "users":
				switch (httpMethod) {
				case "GET":
					if (uriSegments.length > 2) {
						reqData.put("userNo", uriSegments[2]);
						beanName = "userGet";
					}
					break;

				case "POST":
					if (uriSegments.length > 2) {
						if (uriSegments[2].equals("login")) {
							beanName = "userLogin";
						} else if (uriSegments[2].equals("add")) {
							beanName = "userAdd";
						}
					}
					break;

				case "PUT":
					if (uriSegments.length > 2) {
						reqData.put("userNo", uriSegments[2]);
						beanName = "userUpdate";
					}
					break;

				case "DELETE":
					if (uriSegments.length > 2) {
						reqData.put("userNo", uriSegments[2]);
						beanName = "userDelete";
					}
					break;
				default:
					break;
				}
				break;

			case "friends":
				switch (httpMethod) {
				case "GET":
					if (uriSegments.length > 2) {
						reqData.put("userNo", uriSegments[2]);
						beanName = "friendGet";
					}
					break;

				case "POST":
					if (uriSegments.length > 2) {
						reqData.put("userNo", uriSegments[2]);
						beanName = "friendAdd";
					}
					break;

				/*
				 * case "DELETE": beanName = "friendDelete"; break;
				 */

				default:
					break;
				}
				break;

			case "contents":
				switch (httpMethod) {
				case "GET":
					if (uriSegments.length > 2) {
						reqData.put("userNo", uriSegments[2]);
					}
					beanName = "contentsGet";
					break;

				case "POST":
					if (uriSegments.length > 2) {
						reqData.put("contentsNo", uriSegments[2]);
						beanName = "commentAdd";
					} else {
						beanName = "contentsAdd";
					}
					break;

				case "PUT":
					if (uriSegments.length > 2) {
						reqData.put("contentsNo", uriSegments[2]);
						beanName = "readUpdate";
					}
					break;

				default:
					break;
				}
				break;
			}

			if (beanName == null) {
				beanName = "notFound";
			}
			service = (ApiRequest) springContext.getBean(beanName, reqData);

		} catch (Exception e) {
			System.out.println(e);
			service = (ApiRequest) springContext.getBean("notFound", reqData);
		}

		return service;
	}
}
