package tokyo.ejservice.kizuna.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import redis.clients.jedis.Jedis;
import tokyo.ejservice.kizuna.service.dao.TokenKey;

public class TokenHelper {
	/**
	 * log4j logger
	 */
	private static final Logger logger = LogManager.getLogger(TokenHelper.class);
	private static final JedisHelper helper = JedisHelper.getInstance();
	private static final int EXPIRE_TIMESPAN_MS = 3 * 60 * 60 * 1000; //ms, 3hr
	// for singleton
	private static TokenHelper instance = null;

	private TokenHelper() {

	}

	public static TokenHelper getInstance() {
		if (instance == null)
			instance = new TokenHelper();
		return instance;
	}

	public String createToken(String userNo, String email) {
		KeyMaker tokenKey = null;
		Jedis jedis = null;
		try {
			long issueDate = System.currentTimeMillis();

			// token生成
			JsonObject token = new JsonObject();
			token.addProperty("issueDate", issueDate);
			token.addProperty("expireDate", issueDate + EXPIRE_TIMESPAN_MS);
			token.addProperty("email", email);
			token.addProperty("userNo", userNo);

			tokenKey = new TokenKey(email, issueDate);
			jedis = helper.getConnection();
			jedis.setex(tokenKey.getKey(), EXPIRE_TIMESPAN_MS, token.toString());
		} catch (Exception e) {
			logger.error(e);
			return null;
		} finally {
			if (jedis != null) {
				helper.returnResource(jedis);
			}
		}

		return (tokenKey == null) ? null : tokenKey.getKey();
	}

	public boolean verifyToken(String tokenKey, String email, String userNo) {
		boolean isValid = false;
		Jedis jedis = null;
		try {
			jedis = helper.getConnection();
			String tokenString = jedis.get(tokenKey);
			if (tokenString != null) {
				Gson gson = new Gson();
				JsonObject token = gson.fromJson(tokenString, JsonObject.class);

				if(email != null && !email.equals(token.get("email").getAsString())) {
					logger.debug("email:" + email + " is not same.");
					return false;
				}

				if (userNo != null && !userNo.equals(token.get("userNo").getAsString())) {
					logger.debug("userNo:" + userNo + " is not same.");
					return false;
				}

				long nowDate = System.currentTimeMillis();
				long expireDate = token.get("expireDate").getAsLong();
				isValid = (nowDate <= expireDate);
				// update expire date
				if(isValid) {
					token.addProperty("expireDate", nowDate + EXPIRE_TIMESPAN_MS);
					jedis.setex(tokenKey, EXPIRE_TIMESPAN_MS, token.toString());
				}
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			if (jedis != null) {
				helper.returnResource(jedis);
			}
		}
		return isValid;
	}

	public long getExpireMillisec (String tokenKey) {
		long expireMillisec = 0;
		Jedis jedis = null;
		try {
			jedis = helper.getConnection();
			String tokenString = jedis.get(tokenKey);
			if (tokenString != null) {
				Gson gson = new Gson();
				JsonObject token = gson.fromJson(tokenString, JsonObject.class);
				expireMillisec = token.get("expireDate").getAsLong();
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			if (jedis != null) {
				helper.returnResource(jedis);
			}
		}
		return expireMillisec;
	}

	public void expireToken(String tokenKey) {
		Jedis jedis = null;
		try {
			// token削除
			jedis = helper.getConnection();
			long result = jedis.del(tokenKey);
			logger.debug("expireToken: " + result);
		} catch (Exception e) {
			logger.error(e);
		} finally {
			if (jedis != null) {
				helper.returnResource(jedis);
			}
		}
	}
}
