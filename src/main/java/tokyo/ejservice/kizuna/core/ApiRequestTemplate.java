package tokyo.ejservice.kizuna.core;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonObject;

import tokyo.ejservice.kizuna.service.RequestParamException;
import tokyo.ejservice.kizuna.service.ServiceException;

public abstract class ApiRequestTemplate implements ApiRequest {
	// javaのenumは使いづらい。。。
	public static final Map<String, Integer> SEX;
	static {
		SEX = new HashMap<String, Integer>();
		SEX.put("M", 1);
		SEX.put("F", 2);

	}
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static final Map<String, Boolean> BOOL;

	static {
		BOOL = new HashMap<String, Boolean>();
		BOOL.put("Y", true);
		BOOL.put("N", false);
	}

	protected Logger logger;
	/**
	 * APIリクエスト data
	 */
	protected Map<String, String> reqData;

	/**
	 * API処理結果
	 */
	protected JsonObject apiResult;

	/**
	 * logger生成<br/>
	 * apiResultオブジェクト生成
	 */
	public ApiRequestTemplate(Map<String, String> reqData) {
		this.logger = LogManager.getLogger(this.getClass());
		this.apiResult = new JsonObject();
		this.reqData = reqData;

		logger.info("request data : " + this.reqData);
	}

	public void executeService() {
		try {
			this.requestParamValidation();

			this.service();
		} catch (RequestParamException e) {
			logger.error(e);
			this.apiResult.addProperty("resultCode", 405);
			this.apiResult.addProperty("message", e.getMessage());
		} catch (ServiceException e) {
			logger.error(e);
			this.apiResult.addProperty("resultCode", 501);
			this.apiResult.addProperty("message", e.getMessage());
		}
	}

	public JsonObject getApiResult() {
		return this.apiResult;
	}

	/*
	 * @Override public void requestParamValidation() throws
	 * RequestParamException { if (getClass().getClasses().length == 0) {
	 * return; }
	 *
	 * // // TODO 이건 꼼수 바꿔야 하는데.. // for (Object item : //
	 * this.getClass().getClasses()[0].getEnumConstants()) { // RequestParam
	 * param = (RequestParam) item; // if (param.isMandatory() &&
	 * this.reqData.get(param.toString()) == // null) { // throw new
	 * RequestParamException(item.toString() + //
	 * " is not present in request param."); // } // } }
	 */

	public final <T extends Enum<T>> T fromValue(Class<T> paramClass, String paramValue) {
		if (paramValue == null || paramClass == null) {
			throw new IllegalArgumentException(
					"There is no value with name '" + paramValue + " in Enum " + paramClass.getClass().getName());
		}

		for (T param : paramClass.getEnumConstants()) {
			if (paramValue.equals(param.toString())) {
				return param;
			}
		}

		throw new IllegalArgumentException(
				"There is no value with name '" + paramValue + " in Enum " + paramClass.getClass().getName());
	}
}