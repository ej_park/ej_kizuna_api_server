package tokyo.ejservice.kizuna;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.amazonaws.auth.BasicAWSCredentials;

@Configuration
@PropertySource("classpath:amazon-s3-config.properties")
public class AmazonS3Config {
	@Value("${AWSAccessKeyId}")
	private String accessKeyId;

	@Value("${AWSSecretAccessKey}")
	private String secretAccessKey;

	@Value("${AWSRegion}")
	private String region;

	@Value("${Bucket}")
	private String defaultBucketName;

	@Bean(name = "bucketName")
	public String getBucketName() {
		return this.defaultBucketName;
	}

	@Bean(name = "region")
	public String getAwsRegion() {
		return this.region;
	}

	@Bean(name = "Credentials")
	public BasicAWSCredentials getBasicAWSCredentials() {
		return new BasicAWSCredentials(accessKeyId, secretAccessKey);
	}
}
